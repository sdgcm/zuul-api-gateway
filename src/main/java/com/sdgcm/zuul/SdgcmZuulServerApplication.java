package com.sdgcm.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;


@EnableZuulProxy
@EnableDiscoveryClient
@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "Zuul API", version = "2.0", description = "Zuul Microservicio" ))
public class SdgcmZuulServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SdgcmZuulServerApplication.class, args);
	}

}
